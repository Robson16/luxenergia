<?php

/**
 * Kirk Customizer Plugin configurations
 *
 * @link https://kirki.org/
 *
 */

add_action('after_setup_theme', function () {
	if (class_exists('Kirki')) {
		require_once get_template_directory() . '/includes/kirki/kirki-control-footer.php';
	}
}, 20);
