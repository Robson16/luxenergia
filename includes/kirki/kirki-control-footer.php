<?php

/**
 * Kirki customizer - List of Addresses
 *
 */

new \Kirki\Section(
	'luxenergia_section_footer',
	array(
		'title'       => esc_html__('Footer', 'luxenergia'),
		'description' => esc_html__('Extra options to customize the footer.', 'luxenergia'),
		'priority'    => 160,
	)
);

new \Kirki\Field\Image(
	array(
		'settings'    => 'luxenergia_setting_footer_logo',
		'label'       => esc_html__('Logo', 'luxenergia'),
		'section'     => 'luxenergia_section_footer',
		'default'     => '',
		'choices'     => [
			'save_as' => 'array',
		],
	)
);

new \Kirki\Field\URL(
	[
		'settings' => 'luxenergia_setting_footer_main_site',
		'label'    => esc_html__('Main site URL', 'luxenergia'),
		'section'  => 'luxenergia_section_footer',
		'default'  => 'https://www.luxenergia.com.br/',
		'priority' => 10,
	]
);

new \Kirki\Field\Editor(
	array(
		'settings'    => 'luxenergia_setting_address',
		'label'       => esc_html__('Address', 'luxenergia'),
		'section'     => 'luxenergia_section_footer',
		'default'     => '',
		'priority'    => 10,
	)
);

new \Kirki\Field\Text(
	[
		'settings'    => 'luxenergia_setting_email',
		'label'       => esc_html__('E-mail', 'luxenergia'),
		'section'     => 'luxenergia_section_footer',
		'default'     => esc_html('exemple@domain.com'),
		'priority'    => 10,
	]
);

new \Kirki\Field\Text(
	[
		'settings'    => 'luxenergia_setting_phone',
		'label'       => esc_html__('Phone', 'luxenergia'),
		'section'     => 'luxenergia_section_footer',
		'default'     => esc_html('+55 (11) 5555-5555'),
		'priority'    => 10,
	]
);
