<?php

/**
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 */

// Front-End
function luxenergia_scripts()
{
	// CSS
	wp_enqueue_style('luxenergia-shared-styles', get_template_directory_uri() . '/assets/css/shared/shared-styles.css', array(), wp_get_theme()->get('Version'));
	wp_enqueue_style('luxenergia-frontend-styles', get_template_directory_uri() . '/assets/css/frontend/frontend-styles.css', array(), wp_get_theme()->get('Version'));

	// Js
	wp_enqueue_script('comment-reply');
	wp_enqueue_script('luxenergia-frontend-script', get_template_directory_uri() . '/assets/js/frontend/frontend.min.js', array(), wp_get_theme()->get('Version'), true);
}
add_action('wp_enqueue_scripts', 'luxenergia_scripts');

/**
 * Set theme defaults and register support for various WordPress features.
 */
function luxenergia_setup()
{
	// Enabling translation support
	load_theme_textdomain('luxenergia', get_template_directory() . '/languages');

	// Customizable logo
	add_theme_support('custom-logo', array(
		'height'      => 45,
		'width'       => 140,
		'flex-height' => true,
		'flex-width'  => true,
	));

	// Menu registration
	register_nav_menus(array(
		'main_menu' => __('Main Menu', 'luxenergia'),
	));

	// Load custom styles in the editor.
	add_theme_support('editor-styles');
	add_editor_style(get_stylesheet_directory_uri() . '/assets/css/shared/shared-styles.css');

	// Let WordPress manage the document title.
	add_theme_support('title-tag');

	// Enable support for featured image on posts and pages.
	add_theme_support('post-thumbnails');

	// Enable support for embedded media for full weight
	add_theme_support('responsive-embeds');

	// Standard style for each block.
	add_theme_support('wp-block-styles');
}
add_action('after_setup_theme', 'luxenergia_setup');

/**
 * Remove website field from comment form
 */
function luxenergia_website_remove($fields)
{
	if (isset($fields['url']))
		unset($fields['url']);
	return $fields;
}
add_filter('comment_form_default_fields', 'luxenergia_website_remove');

/**
 *  TGM Plugin
 */
require_once get_template_directory() . '/includes/required-plugins.php';

/**
 *  WordPress Bootstrap Nav Walker
 */
require_once get_template_directory() . '/includes/classes/class-wp-bootstrap-navwalker.php';

/**
 *  Kirki Framework Config
 */
require_once get_template_directory() . '/includes/kirki/kirki-config.php';
