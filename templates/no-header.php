<?php

/*
Template Name: No Header
Template Post Type: page
*/

get_header('clean');

while (have_posts()) {
	the_post();
	get_template_part('partials/content/content', 'page');
}

get_footer();
