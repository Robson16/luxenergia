<?php

/*
Template Name: Gradient Background
Template Post Type: page
*/

get_header();

while (have_posts()) {
	the_post();
	get_template_part('partials/content/content', 'page');
}

get_footer();
