<?php

/**
 * The template for displaying the footer
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 */

?>

</main>

<footer id="footer" class="footer">
	<div class="container">
		<?php
		$logo    = get_theme_mod('luxenergia_setting_footer_logo');
		$url     = get_theme_mod('luxenergia_setting_footer_main_site', esc_url('https://www.luxenergia.com.br/'));
		$address = get_theme_mod('luxenergia_setting_address');
		$email   = get_theme_mod('luxenergia_setting_email', esc_html('exemple@domain.com'));
		$phone   = get_theme_mod('luxenergia_setting_phone', esc_html('+55 (11) 5555-5555'));
		?>

		<div class="logo">
			<?php
			if ($logo) {
				echo sprintf(
					'<img src="%s" width="%s" height="%s" alt="%s">',
					esc_url($logo['url']),
					esc_attr($logo['width']),
					esc_attr($logo['height']),
					get_bloginfo('name'),
				);
			}
			?>
		</div>

		<div class="link">
			<?php
			if ($url) echo sprintf(
				'<a href="%s">%s <span>%s</span></a>',
				esc_html($url),
				file_get_contents(get_template_directory_uri() . '/assets/svg/icon-globe.svg'),
				esc_html__('Visit our website', 'luxenergia'),
			);
			?>
		</div>

		<div class="infos">
			<?php if ($address) echo wp_kses_post($address); ?>

			<span>
				<?php
				if ($email)	echo sprintf(
					'<a href="mailto: %s">%s</a>',
					esc_html($email),
					esc_html($email),
				);
				?>

				<br>

				<?php if ($phone) echo sprintf(
					'<a href="tel: %s">%s</a>',
					preg_replace("/[^0-9]/", "", esc_html($phone)),
					esc_html($phone),
				);
				?>
			</span>
		</div>
	</div>
	<!-- /.container -->
</footer>

<?php wp_footer(); ?>

</body>

</html>