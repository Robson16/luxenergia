export default class Header {
	constructor() {
		this.body = document.querySelector( 'body' );
		this.wpadminbar = document.querySelector( '#wpadminbar' );
		this.navbar = document.querySelector( '#navbar' );
		this.viewportX = document.documentElement.clientWidth;
		this.viewportY = document.documentElement.clientHeight;
		this.offset = this.wpadminbar ? this.wpadminbar.offsetHeight : 0;

		this.events();
	}

	events() {
		if ( this.navbar ) {
			[ 'resize', 'load' ].forEach( ( event ) => {
				window.addEventListener( event, () => {
					this.handleViewportSize();
					this.handleWhenLoggedIn();
				} );
			} );

			this.handleNavbarToggle();
		}
	}

	handleWhenLoggedIn() {
		// When the WP Admin Bar is visible
		if ( this.wpadminbar && this.viewportX > 992 ) {
			this.navbar.style.top = `${ this.wpadminbar.offsetHeight }px`;
		} else if ( this.wpadminbar && this.viwewportX > 992 ) {
			this.navbar.style.top = 'initial';
		}
	}

	handleViewportSize() {
		this.viewportX = document.documentElement.clientWidth;
		this.viewportY = document.documentElement.clientHeight;
	}

	handleNavbarToggle() {
		const navbarToggler = this.navbar.querySelector( '.navbar-toggler' );

		if ( navbarToggler ) {
			const icon = navbarToggler.querySelector( '.navbar-toggler-icon' );
			let target = navbarToggler.dataset.target;

			target = document.querySelector( target );

			if ( target ) {
				navbarToggler.addEventListener( 'click', () => {
					this.body.classList.toggle( 'no-scroll-vertical' );

					navbarToggler.classList.toggle( 'show' );
					target.classList.toggle( 'show' );
					icon.classList.toggle( 'close' );
				} );
			}
		}
	}
}
