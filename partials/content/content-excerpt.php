<?php

/**
 * Template part for displaying post archives and search results
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 */

?>

<div id="post-<?php the_ID(); ?>" <?php post_class('post-excerpt'); ?>>
	<header class="post-excerpt__header">
		<a class="post-excerpt__thumbnail" href="<?php echo esc_url(get_permalink()); ?>">
			<?php the_post_thumbnail('thumbnail_excerpt', array('title' => get_the_title())); ?>
		</a>

		<?php
		if (is_sticky() && is_home() && !is_paged()) {
			printf('<span class="sticky-post">%s</span>', _x('Post', 'Featured', 'luxenergia'));
		}

		the_title(sprintf('<h3 class="post-excerpt__title"><a href="%s" rel="bookmark">', esc_url(get_permalink())), '</a></h3>');
		?>

		<p class="post-excerpt__meta">
			<?php echo date_i18n(get_option('date_format'), get_post_timestamp()); ?>

			&#47;

			<?php echo get_the_author_meta('display_name'); ?>

			&#47;

			<?php
			$categories = get_categories(array(
				'orderby' => 'name',
				'parent'  => 0
			));

			foreach ($categories as $category) {
				printf(
					'<a href="%1$s">%2$s</a>, ',
					esc_url(get_category_link($category->term_id)),
					esc_html($category->name)
				);
			}
			?>
		</p>
	</header>
	<!-- /.post-excerpt__header -->

	<div class="post-excerpt__content">
		<?php the_excerpt(); ?>
	</div>
	<!-- /.post-excerpt__content -->

	<footer class="post-excerpt__footer">
		<a class="read-more-link" href="<?php echo esc_url(get_permalink()); ?>">
			<?php _e('Read more', 'luxenergia') ?>
			<i class="icon-arrow-right-up"></i>
		</a>
	</footer>
	<!-- /.post-excerpt__footer -->

</div><!-- #post-<?php the_ID(); ?> -->