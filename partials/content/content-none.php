<?php
/*
 * Template Part to display that no posts were found
 */
?>

<div>
	<h2 class="text-center"><?php _e('No content to display', 'luxenergia'); ?></h2>
</div>